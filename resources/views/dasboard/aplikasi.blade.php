@extends('layouts.dasboard')
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <div class="form-group col-md-2">
        <form action="filter.tahun" method="get">
            <select class="form-control select22" name="tahun" onchange="this.form.submit();">
                <?php 
                $sekrang = date('Y');
                for ($i=$sekrang; $i >= 2017 ; $i--) { 
                    ?>
                    @if($filter == $i)
                    <option value="{{$i}}" selected="true">{{$i}}</option>
                    @else
                    <option value="{{$i}}">{{$i}}</option>
                    @endif
                    <?php
                }

                ?>
            </select>
        </form>
    </div>
    <br>
    <br>
    <div class="m-portlet__body">
        <div id="chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
    <br>
    <br>
    <div class="m-portlet__body">
        <div id="column" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){  
        $(".select22").select2({
            placeholder: "Nama Barang",
            allowClear: true
        });
    });
</script> 
<!-- Start Create The Line Chart -->
<script>
    Highcharts.chart('chart', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Pengeluaran Barang Perbulan'
        },
        xAxis: {
            categories:[
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
            '<td style="padding:0"><b>{point.y:.f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Pengeluaran Barang Perbulan',
                // Mengambil Data Dari Controller 
                data:{!! json_encode($bulans) !!}
            }]
        }); 
    </script>

    <!-- Start Create The Column Chart -->
    <script type="text/javascript">
        // Create the chart
        Highcharts.chart('column', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Jumlah Pemasukan Barang'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total Jumlah Pemasukan Barang'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:.f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b><br/>'
            },

            series: [
            {
                name: "",
                colorByPoint: true,
                data: [
                    // Membuat Perulangan Untuk Menampilkan Barang 
                    <?php foreach ($nama_barang as $key => $value): ?>
                        {
                            name: {!!json_encode($value)!!},
                            // Mengambil Variabel Jumlah untuk Sum Jumlah Total Barang
                            y: {!! json_encode($jumlah->where('nama_barang','=',$value)->sum('jumlah')) !!}
                        },
                    <?php endforeach ?>
                    ]
                }
                ],
            });
        </script>  

        @stop
        <script src="https://code.highcharts.com/highcharts.js"></script>