<script type="text/javascript">
	$('.date').datepicker({  

		format: 'dd-mm-yyyy',
		autoclose: true,
		todayHighlight: true
	});  
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){  
		$(".select22").select2({
			placeholder: "Nama Barang",
			allowClear: true
		});
		var i=1;
		$('.tambah').click(function(){  
			i++;
			$('tbody').append(
				'<tr id="row'+i+'">'+
				
				'<td width="40%"><select style="width: 100%;" class="form-control select22 kedua" id="'+i+'" name="id_barang[]"><option></option>@foreach($barang as $barangs)<option value="{{$barangs->id_barang}}" stoks="{{$barangs->stok}}" harga="{{number_format($barangs->harga)}}"">{{$barangs->nama_barang}}</option>@endforeach</select><input type="hidden" class="nama_barang'+i+'" value=""></td>'+

				'<td width="15%"><input style="width: 100%;" type="text" readonly name="stok[]" class="form-control mb-2 stoks'+i+'" placeholder="Stok"></td>'+

				'<td width="18%"><input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2 number" id="inlineFormInput" placeholder="Jumlah" autocomplete="off"></td>'+

				'<td><input type="text" name="harga[]" id="harga'+i+'" readonly id="harga" class="form-control mb-2" placeholder="Harga"></td>'+

				'<td><a href="#" class="btn btn-sm btn-danger mb-2 remove" id="'+i+'"><i class="fas fa-minus"></i></a></td>'+
				'</tr>'
				);
			$(".select22").select2({
				placeholder: "Nama Barang",
				allowClear: true
			});
			$(document).on('click', '.remove', function(){  
				var button_id = $(this).attr("id");   
				$('#row'+button_id+'').remove();  
			}); 
			$('.number').on('keydown', function(e){
				-1!==$
				.inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
				.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
				|| 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
				&& (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
			});
			$(document).on('change', '.kedua', function(){  
				var select_id = $(this).attr("id"); 
				var stoks = $('#'+select_id+' option:selected').attr('stoks');
				var harga = $('#'+select_id+' option:selected').attr('harga');
				var texts = $('#'+select_id+' option:selected').val();
				$('.stoks'+select_id+'').val(stoks);
				$('#harga'+select_id+'').val(harga);
				$('.nama_barang'+select_id+'').val(texts);
				s = select_id - 1;
				se = select_id - 2;
				sel = select_id - 3;
				sele = select_id - 4;
				selec = select_id - 5;
				select = select_id - 6;
				selecte = select_id - 7;
				selected = select_id - 8;
				selecteda = select_id - 9;
				selectedab = select_id - 10;
				if ($(".nama_barang").val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+s+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+se+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+sel+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+sele+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+selec+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+select+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+selecte+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+selected+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+selecteda+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}else if ($('.nama_barang'+selectedab+'').val() == $('.nama_barang'+select_id+'').val()) {
					Swal.fire({
						type: 'error',
						title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
						text: 'Error! Silahkan Pilih Kembali'
					})
				}
			});
		});
$("#element").on("change", function(){
	var harga = $("#element option:selected").attr("harga");
	var stok = $("#element option:selected").attr("stok");
	var text = $("#element option:selected").val();
	$("#harga").val(harga);
	$(".stok").val(stok);
	$(".nama_barang").val(text);
}); 
$('#number').on('keydown', function(e){
	-1!==$
	.inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
	.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
	|| 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
	&& (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
});
});
</script>