@extends('layouts.excelbarang')
@section('content')
@include('layouts.alert_form_null')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header"></div>

				<div class="card-body">
					<form method="get" action="export_excel.export">
						@csrf
						<div class="form-group row">
							<label class="col-md-4 col-form-label text-md-right">Barang</label>
							<div class="col-md-6">
								<select name="id_barang" class="form-control select22 input-lg dynamic"data-dependent="tanggal" id="jenis">
									<option></option>
									@foreach($barang as $nama)
									<option value="{{$nama->id_barang}}">{{$nama->nama_barang}}</option>
									@endforeach
								</select>
							</div>

						</div>

						<div class="form-group row">
							<label class="col-md-4 col-form-label text-md-right">Tanggal Awal</label>

							<div class="col-md-6">
								<input readonly type="" name="tanggal" id="dates" class="form-control" autocomplete="off">  
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-4 col-form-label text-md-right">Tanggal Akhir</label>

							<div class="col-md-6">
								<input readonly type="" name="tanggal_akhir" id="date" class="form-control" autocomplete="off">
							</div>
						</div>
						<!-- <input type="text" name="id_transaksi" id="id_transaksi"> -->
						<div class="form-group row mb-0">
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-sm btn-primary">Cetak</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#dates').datepicker({  

		format: 'dd-mm-yyyy',
		autoclose: true,
		todayHighlight: true
	});
	$('#date').datepicker({  

		format: 'dd-mm-yyyy',
		autoclose: true,
		todayHighlight: true
	});  
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".select22").select2({
			placeholder: "Nama Barang",
			allowClear: true
		});
	});
</script>
@endsection