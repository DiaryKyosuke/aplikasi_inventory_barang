@extends('layouts.masteruser')
@section('title','Data User')
@section('masteruser')
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
	@if ($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{$message}}</p>
	</div>
	@endif
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data User
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{url('master.user.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah User</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
			<thead >
				<tr class="text-center">
					<th width="5%">No</th>
					<th>Username</th>
					<th>Email</th>
					<th>Role</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr id="{{++$i}}">
					<td>{{$i}}.</td>
					<td>{{$user->username}}</td>
					<td>{{$user->email}}</td>
					<td class="text-center">{{$user->role}}</td>					
					<td class="text-center">{{$user->status}}</td>
					<td class="text-center">
						<form method="get" action="master.user.{{$user->id_user}}.reset" class="test{{$i}}">
						<a href="#" class="btn btn-outline-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air reset{{$i}}" data-container="body" data-toggle="m-popover" data-placement="left" data-content="Reset Password"><i class="la la-history"></i></a>
						
						<a href="master.user.{{$user->id_user}}.edit_user" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" data-container="body" data-toggle="m-popover" data-placement="top" data-content="Edit Data User"><i class="fa fa-user-edit"></i></a>

						<a href="master/user/{{$user->id_user}}/destroy" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus')" data-container="body" data-toggle="m-popover" data-placement="right" data-content="Hapus User"><i class="fa fa-trash"></i></a>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){ 
	<?php foreach ($users as $user): ?>
		var q{{++$d}} = {{$b++}};
	 	$('.reset'+q{{$d}}+'').click(function(){  
			Swal.fire({
				title: 'Apakah Anda Yakin Untuk Mereset Password?',
				text: "Reset Password Anda!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, Reset Password!',
				cancelButtonText: 'Tidak, Cancel!',
			}).then((result) => {
				if (result.value) {
					$('.test'+q{{$d}}+'').submit();
				}
			});
		});
	 <?php endforeach ?> 
		
	});
</script>
@endsection