<table>
	<thead>
		<tr>
			<th width="20char">No.</th>
			<th width="30%">Nama Barang</th>
			<th width="20%">Tanggal</th>
			<th width="20%">Harga</th>
			<th width="10%">Jumlah</th>
			<th width="40%">Total</th>
		</tr>
	</thead>
	<tbody>
		@foreach($dets as $det)
		<tr>
			<td>{{++$i}}</td>
			<td>{{$det->nama_barang}}</td>
			<td>{{ Carbon\Carbon::parse($det->tanggal)->formatLocalized('%A, %d %B %Y')}}</td>
			<td>Rp. {{number_format($det->harga)}}</td>
			<td>{{$det->jumlah}}</td>
			<td>
				<?php
				$harga = $det->harga;
				$jumlah = $det->jumlah;
				echo ($harga * $jumlah);
				?>
			</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Total </td>
			<td>=SUM(F2:F{{$i+1}})</td>
		</tr>
	</tbody>
</table>