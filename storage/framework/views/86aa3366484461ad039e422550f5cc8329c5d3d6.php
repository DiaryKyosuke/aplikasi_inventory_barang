<table>
	<thead>
		<tr>
			<th width="20char">No.</th>
			<th width="30%">Nama Barang</th>
			<th width="20%">Tanggal</th>
			<th width="20%">Harga</th>
			<th width="10%">Jumlah</th>
			<th width="40%">Total</th>
		</tr>
	</thead>
	<tbody>
		<?php $__currentLoopData = $dets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td><?php echo e(++$i); ?></td>
			<td><?php echo e($det->nama_barang); ?></td>
			<td><?php echo e(Carbon\Carbon::parse($det->tanggal)->formatLocalized('%A, %d %B %Y')); ?></td>
			<td>Rp. <?php echo e(number_format($det->harga)); ?></td>
			<td><?php echo e($det->jumlah); ?></td>
			<td>
				<?php
				$harga = $det->harga;
				$jumlah = $det->jumlah;
				echo ($harga * $jumlah);
				?>
			</td>
		</tr>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Total </td>
			<td>=SUM(F2:F<?php echo e($i+1); ?>)
			</td>
		</tr>
	</tbody>
</table>