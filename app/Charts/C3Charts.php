<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\C3\Chart;

class C3Charts extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}
