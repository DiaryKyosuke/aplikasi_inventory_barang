<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
	Protected $table ='transaksi';
	Protected $primaryKey ='id_transaksi';
	Protected $fillable =['tanggal','keterangan','jenis','permission'];
}
