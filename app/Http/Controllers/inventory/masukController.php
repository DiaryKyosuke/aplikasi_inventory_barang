<?php

namespace App\Http\Controllers\inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\transaksi;
use App\detail;
use App\Barang;
use Illuminate\Support\Facades\DB;
use Alert;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class masukController extends Controller
{
            /**
     * Create a new controller instance.
     *
     * @return void
     */
            public function __construct()
            {
                $this->middleware('auth');
            }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $b = 1;
        $d = 0;
        $transaksi = DB:: table('transaksi')
        ->where('jenis','=',1)
        ->latest()
        ->paginate();
        $akses = DB::table('user')
        ->join('users','users.id','=','user.id_user')
        ->where('user.username','=',Auth::user()->username)
        ->first();
        return view('inventory/masuk/grid', compact('transaksi','akses','b','d'))
        ->with('i',(request()->input('page',1) -1)*5);
    }
    
    public function lock(Request $request, $id_transaksi)
    {
       $this->validate($request, [
        'permission' => 'required'
    ]);
               // dibawah ini untuk mengupdate stok yang telah ditambah
        if (count($request->id_barang) > 0 ){
        foreach ($request->id_barang as $item => $v) {
        $stok = $request->stok[$item];
        $jumlah = $request->jumlah[$item];
        $total = $stok + $jumlah;
        $barang= array(
        'stok' => $total
        );
        DB::table('barang')->where('id_barang','=', $request->id_barang[$item])->update($barang);
      }
    }
       $transaksi = transaksi::find($id_transaksi);
       $transaksi->permission = $request->get('permission');
       $transaksi->save();
       return redirect('inventory.masuk.grid');


   }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang =\App\Barang::latest()->paginate();
         $akses = DB::table('user')
        ->join('users','users.id','=','user.id_user')
        ->where('user.username','=',Auth::user()->username)
        ->first();
        return view('inventory/masuk/form', compact('barang','akses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $transaksi = transaksi::create([
            'tanggal' => Carbon::parse($request->tanggal)->formatLocalized('%Y-%m-%d'),
            'keterangan' => request('keterangan'),
            'jenis' => request('jenis'),
            'permission' => request('permission')
        ])->id_transaksi;

        if (count($request->id_barang) > 0 ){
            foreach ($request->id_barang as $item => $v) {
                $detail= array(
                    $harga = $request->harga[$item],
                    $harga_str = preg_replace("/[^0-9]/", "", $harga),
                    'id_transaksi' => $transaksi,
                    'id_barang'=> $request->id_barang[$item], 
                    'harga'=> $harga_str, 
                    'jumlah'=> $request->jumlah[$item]
                );
                detail::create($detail);
            }
        }

      Alert::success('Data Yang Anda Inputkan Berhasil Di Tambahkan','Tersimpan')->persistent("OK");
      return redirect('inventory.masuk.grid');
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_transaksi)
    {
        $transaksis = transaksi::find($id_transaksi);
        /*jumlah form sesuai dengan id_transaksi di detail_transaksi (perulangan)*/
        $dets = DB::table('detail_transaksi')
        ->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
        ->where('id_transaksi','=', $id_transaksi)->paginate();
        return view('inventory/masuk/view',compact('transaksis','details','dets'))->with('i',(request()->input('page',1) -1)*5);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_transaksi)
    {
        /*ini untuk tanggal dan keterangan*/
        $transaksis = transaksi::find($id_transaksi);
        /*yang ini untuk menampilkan jumlah dan harga*/
        $details = detail::find($id_transaksi);
        /*jumlah form sesuai dengan id_transaksi di detail_transaksi (perulangan)*/
        $dets = DB::table('detail_transaksi')
        ->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
        ->where('id_transaksi','=', $id_transaksi)->paginate();
        /*dibawah ini untuk menampilkan harga dan stok di js*/
        $barangs = Barang::find($details->id_barang);
        /*yang dibawah ini digunakan untuk perulangan di option select2*/
        $bars = Barang::find($details->id_barang)->paginate();
        // yang ini saya gunakan untuk membuat deklarasi sesuai nomor urut perulangan
        $b = 1;
        $c = 1;
        $d = 0;
        $hapus = 0;
        $terhapus = 1;
        // ini untuk select2 di append
        $benda =\App\Barang::latest()->paginate();
        $akses = DB::table('user')
                ->join('users','users.id','=','user.id_user')
                ->where('user.username','=',Auth::user()->username)
                ->first();
        return view('inventory.masuk.edit',compact('transaksis','details','barangs','bars', 'dets' ,'b','c','d','benda','hapus','terhapus','akses'))->with('i');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_transaksi)
    {
    $transaksi = transaksi::create([
        'tanggal' => Carbon::parse($request->tanggal)->formatLocalized('%Y-%m-%d'),
        'keterangan' => request('keterangan'),
        'jenis' => request('jenis'),
        'permission' => request('permission')
    ])->id_transaksi;

    if (count($request->id_barang) > 0 ){
        foreach ($request->id_barang as $item => $v) {
            $detail= array(
                $harga = $request->harga[$item],
                $harga_str = preg_replace("/[^0-9]/", "", $harga),
                'id_transaksi' => $transaksi,
                'id_barang'=> $request->id_barang[$item], 
                'harga'=> $harga_str, 
                'jumlah'=> $request->jumlah[$item]
            );
            detail::create($detail);
        }
    }
  $trans = transaksi::find($id_transaksi);
  $trans->delete();




  Alert::success('Data Berhasil Di Update','Tersimpan')->persistent("OK");
  return redirect('inventory.masuk.grid');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_transaksi)
    {
        $transaksi = transaksi::find($id_transaksi);
        $transaksi->delete();
        Alert::success('Data Berhasil Di Hapus','Terhapus')->persistent("OK");
        return redirect('inventory.masuk.grid');
    }
}
