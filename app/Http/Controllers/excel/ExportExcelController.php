<?php

namespace App\Http\Controllers\excel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\transaksi;
use App\detail;
use App\Barang;
use Illuminate\Support\Facades\DB;
use Alert;
use App\Exports\detailExport;
use App\Exports\BarangExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class ExportExcelController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = DB:: table('transaksi')
        ->latest()
        ->paginate();
         $akses = DB::table('user')
        ->join('users','users.id','=','user.id_user')
        ->where('user.username','=',Auth::user()->username)
        ->first();
        return view('export_excel',compact('transaksi','akses'));
    }
    public function index2()
    {
        $barang = DB:: table('barang')
        ->latest()
        ->paginate();
         $akses = DB::table('user')
        ->join('users','users.id','=','user.id_user')
        ->where('user.username','=',Auth::user()->username)
        ->first();
        return view('export_excelbarang',compact('barang','akses'));
    }

    public function export(Request $request) 
    {
        $this->validate($request,[
            'id_barang' => 'required',
            'tanggal' => 'required',
            'tanggal_akhir' => 'required'
        ]);
        if (Carbon::parse($request->tanggal)->formatLocalized('%Y%m%d') > Carbon::parse($request->tanggal_akhir)->formatLocalized('%Y%m%d')) {
            Alert::error('Tanggal Awal Tidak Boleh Melebihi Tanggal Akhir!!','Error, Inputkan Kembali')->persistent("OK");
            return redirect('export_excelbarang');
        }else{
            return Excel::download(new BarangExport($request->tanggal,$request->tanggal_akhir,$request->id_barang), 'Cetak Laporan.xlsx');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'jenis' => 'required',
            'tanggal' => 'required',
            'tanggal_akhir' => 'required'
        ]);
        if (Carbon::parse($request->tanggal)->formatLocalized('%Y%m%d') > Carbon::parse($request->tanggal_akhir)->formatLocalized('%Y%m%d')) {
            Alert::error('Tanggal Awal Tidak Boleh Melebihi Tanggal Akhir!!','Error, Inputkan Kembali')->persistent("OK");
            return redirect('export_excel');
        }else{
         return Excel::download(new detailExport($request->tanggal,$request->tanggal_akhir,$request->jenis), 'Cetak Laporan.xlsx');
     }
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
