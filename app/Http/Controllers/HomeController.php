<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use App\Barang;
use App\transaksi;
use Illuminate\Support\Facades\DB;
use App\Charts\Charts;
use App\Charts\C3Charts;
use App\Charts\ChartjsCharts;
use App\Charts\frappechart;
use App\Charts\FusionCharts;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      
      $test = DB::table('users')
      ->join('user','user.username','=','users.username')
      ->where('user.username','=',Auth::user()->username)
      ->first();

      if ($test->status == 0) {
       Alert::error('Login Anda Gagal, User Tidak Aktif!!','Failed')->persistent("OK");
       Auth::logout();
       return Redirect('login');
     }else{
      Alert::success('Anda Berhasil Login','selamat')->persistent("OK");
      return Redirect('dasboard.aplikasi');
    }
  }
    public function index2(Request $request)
    {
     $filter = $request->get('tahun');
     $saiki = date('Y');
    //Mengambil Nilai Data Perbulan 
     $transaksi = DB::select(
      'SELECT 
      extract(month from transaksi.tanggal) as month,
      count(*) as count
      FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0
      GROUP BY
      1,extract(month from transaksi.tanggal)');

     if ($filter) {
       $jan = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"=0 AND extract(year from transaksi.tanggal) = '.$filter.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 1');
       foreach ($jan as $key => $j) {}

        $feb = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"=0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 2');
      foreach ($feb as $key => $f) {}

        $mar = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 3');
      foreach ($mar as $key => $m) {}

        $apr = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 4');
      foreach ($apr as $key => $a) {}

        $mei = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 5');
      foreach ($mei as $key => $i) {}

        $jun = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 6');
      foreach ($jun as $key => $n) {}

        $jul = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 7');
      foreach ($jul as $key => $l) {}

        $ags = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 8');
      foreach ($ags as $key => $s) {}

        $sep = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 9');
      foreach ($sep as $key => $p) {}

        $okt = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 10');
      foreach ($okt as $key => $t) {}

        $nop = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 11');
      foreach ($nop as $key => $o) {}

        $des = DB::select('SELECT extract(month from transaksi.tanggal) as month,
          count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$filter.' 
          GROUP BY
          1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 12');
      foreach ($des as $key => $e) {}
    } else {
     $jan = DB::select('SELECT extract(month from transaksi.tanggal) as month,
      count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"=0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
      GROUP BY
      1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 1');
     foreach ($jan as $key => $j) {}

      $feb = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"=0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 2');
    foreach ($feb as $key => $f) {}

      $mar = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 3');
    foreach ($mar as $key => $m) {}

      $apr = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 4');
    foreach ($apr as $key => $a) {}

      $mei = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 5');
    foreach ($mei as $key => $i) {}

      $jun = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 6');
    foreach ($jun as $key => $n) {}

      $jul = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 7');
    foreach ($jul as $key => $l) {}

      $ags = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 8');
    foreach ($ags as $key => $s) {}

      $sep = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 9');
    foreach ($sep as $key => $p) {}

      $okt = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 10');
    foreach ($okt as $key => $t) {}

      $nop = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 11');
    foreach ($nop as $key => $o) {}

      $des = DB::select('SELECT extract(month from transaksi.tanggal) as month,
        count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$saiki.' 
        GROUP BY
        1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 12');
    foreach ($des as $key => $e) {}
  }


$trans = transaksi::where('jenis','=',0)->count();
if ($trans == 0) {
  $bulans = ['','','','','','','','','','','',''];
} else {
  foreach ($transaksi as $key => $value) 
  {
    if ($jan == null) {
      $jan = '';
    }else{
      $jan = $j->count;
    }

    if ($feb == null) {
      $feb = '';
    }else{
      $feb = $f->count;
    }

    if ($mar == null) {
      $mar = '';
    }else{
      $mar = $m->count;
    }

    if ($apr == null) {
      $apr = '';
    }else{
      $apr = $a->count;
    }

    if ($mei == null) {
      $mei = '';
    }else{
      $mei = $i->count;
    }

    if ($jun == null) {
      $jun = '';
    }else{
      $jun = $n->count;
    }

    if ($jul == null) {
      $jul = '';
    }else{
      $jul = $l->count;
    }

    if ($ags == null) {
      $ags = '';
    }else{
      $ags = $s->count;
    }

    if ($sep == null) {
      $sep = '';
    }else{
      $sep = $p->count;
    }

    if ($okt == null) {
      $okt = '';
    }else{
      $okt = $t->count;
    }

    if ($nop == null) {
      $nop = '';
    }else{
      $nop = $o->count;
    }

    if ($des == null) {
      $des = '';
    }else{
      $des = $e->count;
    }

    $bulans = [$jan,$feb,$mar,$apr,$mei,$jun,$jul,$ags,$sep,$okt,$nop,$des];
  }
}




//Mengambil Jumlah Dan Jenis 
if ($filter) {
  $jumlah = DB::table('detail_transaksi')
  ->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
  ->join('transaksi','transaksi.id_transaksi','=','detail_transaksi.id_transaksi')
  ->where('transaksi.jenis','=',1)
  ->whereYear('transaksi.tanggal', $filter)
  ->get();
} else {
  $jumlah = DB::table('detail_transaksi')
  ->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
  ->join('transaksi','transaksi.id_transaksi','=','detail_transaksi.id_transaksi')
  ->where('transaksi.jenis','=',1)
  ->whereYear('transaksi.tanggal', $saiki)
  ->get();
}
//Perulangan Untuk Menampilkan Nama Barang
$barang = Barang::all();
$nama_barang = [];
foreach ($barang as $key) {
  $nama_barang[] = $key->nama_barang;
}
$akses = DB::table('user')
->join('users','users.id','=','user.id_user')
->where('user.username','=',Auth::user()->username)
->first();

return view('dasboard.aplikasi',compact('bulans','nama_barang','jumlah','filter','akses'));
}

}
