<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model
{
    Protected $table ='user';
    Protected $primaryKey ='id_user';
    Protected $fillable =['username','email','role','status'];
}
